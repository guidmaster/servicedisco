﻿using Consul;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServiceDisco
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new ConsulClient();

            var service1 = new AgentServiceRegistration();
            service1.ID = "service1";
            service1.Name = "service1";
            service1.Address = "localhost";
            service1.Port = 11000;

            var service2 = new AgentServiceRegistration();
            service2.ID = "service2";
            service2.Name = "service1";
            service2.Address = "127.0.0.1";
            service2.Port = 11001;

            var service3 = new AgentServiceRegistration();
            service3.ID = "service3";
            service3.Name = "service1";
            service3.Address = "remotehost";
            service3.Port = 11002;

            var r = client.Agent.ServiceRegister(service1).Result;
            var r1 = client.Agent.ServiceRegister(service2).Result;
            var r2 = client.Agent.ServiceRegister(service3).Result;




            var lookupClient = new LookupClient(IPAddress.Parse("127.0.0.1"), 8600);

            lookupClient.UseCache = true;
            lookupClient.Retries = 10;
            lookupClient.MinimumCacheTimeout = TimeSpan.FromSeconds(1);


            for (int i = 0; i < 100; i++)
            {
                IDnsQuery dns;
                var result = lookupClient.ResolveServiceAsync("service.consul", "service1").Result;

                if (result.Length > 0)
                {
                    var regi = result.First();
                    Console.WriteLine("Using http://" + regi.AddressList.First().ToString() + ":" + regi.Port) ;
                }


            //    if (result.Length > 0)
            //    {
            //        foreach (var item in result)
            //        {
            //            if (item.AddressList.Length > 0)
            //            {
            //                foreach (var ip in item.AddressList)
            //                {
            ////                    Console.WriteLine(ip.ToString() + item.Port);
            //                    Console.WriteLine("Using http://" + ip.ToString() + ":" + item.Port );
            //                }
            //            }
            //        }
            //    }
                //Console.WriteLine("Done");
                Thread.Sleep(500);
            }


            Console.ReadLine();
        }
    }
}
